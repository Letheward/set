#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>



/* ==== Modules ==== */

#include "base.c"
#include "set.c"
#include "12tet.c"
#include "midi_exporter.c"




/* ==== Main ==== */

Set get_random_set(u64 size) {
    return (Set) { (u64) rand() % (ONE_64 << size), size };
}

// todo: cleanup
u64 print_set_all_modes(Set s) {

    assert(s.size == 12);

    printf("\n[%.4llu]\n", s.data);

    u64 modes[64] = {0};
    u64 count = set_get_all_modes(s, modes);

    for (u64 i = 0; i < count; i++) {
        print_set_traditional((Set) { modes[i], 12 });
    }

    return count;
}

void print_operations(Set a, Set b) {

    typedef Set Operation(Set a, Set b);

    struct { Operation* op; u8 c; u8 commutative; u8 error_on_zero; } ops[] = {
        { set_add, '+', 1, 0 },
        { set_sub, '-', 0, 0 },
        { set_mul, '*', 1, 0 },
        { set_div, '/', 0, 1 },
        { set_mod, '%', 0, 1 },
        { set_and, '&', 1, 0 },
        { set_or,  '|', 1, 0 },
        { set_xor, '^', 1, 0 },
    };

    printf("a =====> "); print_set_12tet(a);
    printf("b =====> "); print_set_12tet(b);

    printf("!a ====> "); print_set_12tet(set_not(a));
    printf("!b ====> "); print_set_12tet(set_not(b));

    for (u64 i = 0; i < count_of(ops); i++) {
        if (ops[i].error_on_zero && b.data == 0) continue;
        printf("a %c b => ", ops[i].c);
        print_set_12tet(ops[i].op(a, b));
    }

    for (u64 i = 0; i < count_of(ops); i++) {
        if (ops[i].commutative || (ops[i].error_on_zero && a.data == 0)) continue;
        printf("b %c a => ", ops[i].c);
        print_set_12tet(ops[i].op(b, a));
    }

    printf("\n");
}


void test_operations() {

    printf("\n\n==== Operations ====\n\n");
    {
        Set a = set_make(12, {0, 4, 7});
        Set b = set_make(12, {0, 3, 7});

        print_set_12tet(a);
        print_set_12tet(set_rotate(a, 2));
        print_set_12tet(set_mirror(a));
        print_operations(a, b);
    }

    for (u64 i = 0; i < 3; i++) {
        Set a = get_random_set(12);
        Set b = get_random_set(12);
        print_operations(a, b);
    }
}


void export_midi_example() {

    MIDI_Exporter exporter = {0};
    midi_exporter_start(&exporter, 1024, 480);

    for (u64 i = 0; i < SET_12TET_COUNT; i++) {
        Set s = {i, 12};
        midi_exporter_append_set_arp(&exporter, s, 8);
        midi_exporter_append_set_chord(&exporter, s, 2);
    }

    midi_exporter_end(&exporter);
    save_file(midi_exporter_string(&exporter), "test.mid");
}


void print_subset_size_stats() {

    printf("\n\n==== Subset Size Stats ====\n\n");

    u64 table[13] = {0};
    for (u64 i = 0; i < SET_12TET_COUNT; i++) {
        Set s = {i, 12};
        table[set_get_size(s)]++;
    }

    for (u64 i = 0; i < count_of(table); i++) {
        printf("%2llu: %4llu\n", i, table[i]);
    }
}


void print_prime_forms_and_modes() {

    printf("\n\n==== Prime Forms and Modes ====\n\n");
    u8 table[SET_12TET_COUNT] = {0};
    for (u64 i = 0; i < SET_12TET_COUNT; i++) {
        table[set_to_prime_form((Set) {i, 12}).data] = 1;
    }

    u64 sorted_table[13][128] = {0};
    u64 set_counts[13] = {0};

    u64 prime_form_count = 0;
    for (u64 i = 0; i < SET_12TET_COUNT; i++) {

        if (table[i]) {

            u64 size = set_get_size((Set) {i, 12});
            assert(set_counts[size] < 128);

            sorted_table[size][set_counts[size]] = i;

            set_counts[size]++;
            prime_form_count++;
        }
    }

    u64 total_mode_count = 0;
    for (u64 i = 0; i < count_of(set_counts); i++) {

        u64 set_count = set_counts[i];
        printf("\n---- Size %llu: %llu prime forms----\n", i, set_count);

        for (u64 j = 0; j < set_count; j++) {
            Set prime = {sorted_table[i][j], 12};
            total_mode_count += print_set_all_modes(prime);
        }
    }

    assert(total_mode_count == 2048);
    printf("\nTotal prime forms: %llu\n", prime_form_count);
    printf("Total modes: %llu\n", total_mode_count);
}

void print_normal_sets_sorted_by_pv() {

    printf("\n\n==== Normal Sets sorted by PV ====\n\n");

    s64 weighting[12] = {0, 0, 5, 4, 3, 2, 1, -5, -4, -3, -2, -1};
    set_pv_weighting_ic7_order_to_index_order(weighting);

    u64 sorted_table[31][256] = {0};
    u64 set_counts[31] = {0};

    for (u64 i = 0; i < SET_12TET_COUNT; i++) {

        if (!(i & 1)) continue;

        s64 pv = set_get_pv_from_weighting((Set) {i, 12}, weighting);

        s64 index = pv + 15;
        assert(index >= 0 && index < (s64) count_of(set_counts));
        assert(set_counts[index] < 256);

        sorted_table[index][set_counts[index]] = i;
        set_counts[index]++;
    }

    for (u64 i = 0; i < count_of(set_counts); i++) {
        for (u64 j = 0; j < set_counts[i]; j++) {
            Set s = {sorted_table[i][j], 12};
            printf("%lld | ", ((s64) i) - 15);
            print_set_12tet(s);
        }
    }
}


void print_all_matched_normal_sets(u8 (*match)(Set), char* title) {

    printf("\n\n==== %s ====\n\n", title);

    u64 count = 0;
    for (u64 i = 0; i < SET_12TET_COUNT; i++) {
        Set s = {i, 12};
        if (s.data & 1 && match(s)) {
            print_set_12tet(s);
            count++;
        }
    }

    printf("Total: %llu\n", count);
}

void print_close_to_major_sets() {

    printf("\n\n==== Sets That Are Close to Major ====\n\n");

    Range ranges[7] = {
        {  0, 0 },
        { -1, 0 },
        { -1, 0 },
        { -1, 1 },
        { -1, 1 },
        { -1, 0 },
        { -1, 0 },
    };

    Set major = set_make(12, {0, 2, 4, 5, 7, 9, 11});

    u64 count = 0;
    for (u64 i = 0; i < SET_12TET_COUNT; i++) {
        Set s = {i, 12};
        if (set_is_close_to(s, major, ranges, count_of(ranges))) {
            print_set_traditional(s);
            count++;
        }
    }

    printf("Total: %llu\n", count);
}





int main() {

    char buffer[1024] = {0};
    setvbuf(stdout, buffer, _IOFBF, sizeof(buffer));

    // test_operations();
    // export_midi_example();
    // print_subset_size_stats();
    // print_prime_forms_and_modes();
    // print_normal_sets_sorted_by_pv();

    // print_all_matched_normal_sets(set_is_neutral,        "Neutral Sets");
    // print_all_matched_normal_sets(set_is_sparse,         "Sparse Sets");
    // print_all_matched_normal_sets(set_is_pure_tertian,   "Pure Tertitan Sets");
    // print_close_to_major_sets();

    fflush(stdout);
}

