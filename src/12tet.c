/* ==== Utils ==== */

#define SET_12TET_COUNT   (ONE_64 << 12)



/* ==== 12TET Set Descriptions ==== */

Description description_table_12tet[SET_12TET_COUNT] = {

    // Trivial Sets
    [   0] = { "Empty Set" },
    [   1] = { "Root" },
    [4095] = { "Chromatic Scale" },

    // Intervals
    [   3] = { "Minor 2nd" },
    [   5] = { "Major 2nd" },
    [   9] = { "Minor 3rd" },
    [  17] = { "Major 3rd" },
    [  33] = { "Pure 4th" },
    [  65] = { "Augmented 4th / Diminished 5th" },
    [ 129] = { "Pure 5th" },
    [ 257] = { "Minor 6th" },
    [ 513] = { "Major 6th" },
    [1025] = { "Minor 7th" },
    [2049] = { "Major 7th" },

    // Chords
    [ 137] = { "Minor Triad" },
    [ 145] = { "Major Triad" },
    [  73] = { "Diminished Triad" },
    [ 273] = { "Augmented Triad" },
    [ 133] = { "Suspended 2nd Chord" },
    [ 161] = { "Suspended 4th Chord" },

    [2193] = { "Major 7th Chord" },
    [1161] = { "Minor 7th Chord" },
    [1169] = { "Dominant 7th Chord" },
    [ 585] = { "Diminished 7th Chord" },
    [1097] = { "Half-Diminished 7th Chord" },
    [2185] = { "Minor Major 7th Chord" },
    [1297] = { "Augmented 7th Chord" },
    [2321] = { "Augmented Major 7th Chord" },
    [2121] = { "Diminished Major 7th Chord" },
    [1105] = { "Dominant 7th b5 Chord" },

    // Symmetric Scales
    [1365] = { "Whole Tone Scale" },
    [1755] = { "Half-Whole Diminished Scale" },
    [2925] = { "Whole-Half Diminished Scale" },
    [2457] = { "Third-Half Augmented Scale" },
    [ 819] = { "Half-Third Augmented Scale" },

    // Major Pentatonic Modes
    [ 661] = { "Major Pentatonic" },
    [1189] = { "Suspended Pentatonic", "2nd mode of Major Pentatonic" },
    [1321] = { "Blues Minor", "3rd mode of Major Pentatonic" },
    [ 677] = { "Blues Major", "4th mode of Major Pentatonic" },
    [1193] = { "Minor Pentatonic", "5th mode of Major Pentatonic" },

    // Ionian Modes
    [2741] = { "Ionian", "the Major Scale" },
    [1709] = { "Dorian", "2nd mode of Major Scale" },
    [1451] = { "Phrygian", "3rd mode of Major Scale"},
    [2773] = { "Lydian", "4th mode of Major Scale" },
    [1717] = { "Mixolydian", "5th mode of Major Scale" },
    [1453] = { "Aeolian", "the minor Scale, 6th mode of Major Scale"},
    [1387] = { "Locrian", "7th mode of Major Scale" },

    // Melodic Minor Modes
    [2733] = { "Melodic Minor", "Minor with natural 6th and 7th" },
    [1707] = { "Dorian b2", "2nd mode of Melodic Minor" },
    [2901] = { "Lydian Augmented" , "3rd mode of Melodic Minor"},
    [1749] = { "Lydian Dominant", "4th mode of Melodic Minor" },
    [1461] = { "Mixolydian b6", "5th mode of Melodic Minor" },
    [1389] = { "Locrian Natural 2", "6th mode of Melodic Minor" },
    [1371] = { "Altered Scale", "7th mode of Melodic Minor" },

    // Harmonic Minor Modes
    [2477] = { "Harmonic Minor", "Minor with a natural 7th" },
    [1643] = { "Locrian Natural 6", "2nd mode of Harmonic Minor" },
    [2869] = { "Ionian #5", "3rd mode of Harmonic Minor" },
    [1741] = { "Ukrainian Dorian", "4th mode of Harmonic Minor" },
    [1459] = { "Phrygian Dominant", "5th mode of Harmonic Minor" },
    [2777] = { "Lydian #2", "6th mode of Harmonic Minor" },
    [ 859] = { "Ultra Locrian", "7th mode of Harmonic Minor" },

    // Harmonic Major Modes
    [2485] = { "Harmonic Major", "Major with a b6" },
    [1645] = { "Dorian b5", "2nd mode of Harmonic Major" },
    [1435] = { "Phrygian b4", "3rd mode of Harmonic Major" },
    [2765] = { "Melodic Minor #4", "4th mode of Harmonic Major" },
    [1715] = { "Mixolydian b2", "5th mode of Harmonic Major" },
    [2905] = { "Lydian Augmented #2", "6th mode of Harmonic Major" },
    [ 875] = { "Locrian bb7", "7th mode of Harmonic Major" },

    // Hungarian Major Modes
    [ 731] = { "Ultra Locrian bb6", "2nd mode of Hungarian Major" },
    [2413] = { "Harmonic Minor b5", "3rd mode of Hungarian Major" },
    [1627] = { "Altered Natural 6", "4th mode of Hungarian Major" },
    [2861] = { "Melodic Minor #5", "5th mode of Hungarian Major" },
    [1739] = { "Ukrainian Dorian b2", "6th mode of Hungarian Major" },
    [2917] = { "Lydian Augmented #3", "7th mode of Hungarian Major" },
    [1753] = { "Hungarian Major", "Half-Whole Diminished without b2" },

    // Romanian Major Modes
    [ 877] = { "Aeolian b5 bb7", "3rd mode of Romanian Major" },
    [1243] = { "Altered bb6", "4th mode of Romanian Major" },
    [2669] = { "Melodic Minor b5", "5th mode of Romanian Major" },
    [1691] = { "Dorian b2 b4", "6th mode of Romanian Major" },
    [2893] = { "Lydian Augmented b3", "7th mode of Romanian Major" },
    [1747] = { "Romanian Major", "Half-Whole Diminished without b3" },
    [2921] = { "Lydian Augmented #2 #3", "2nd mode of Romanian Major" },

    // Double Harmonic Major modes
    [2483] = { "Double Harmonic Major", "Major with b2 and b6" },
    [3289] = { "Lydian #2 #6", "2nd mode of Double Harmonic Major" },
    [ 923] = { "Ultra Phrygian", "3rd mode of Double Harmonic Major" },
    [2509] = { "Double Harmonic Minor", "4th mode of Double Harmonic Major" },
    [1651] = { "Mixolydian b2 b5", "5th mode of Double Harmonic Major" },
    [2873] = { "Ionian #2 #5", "6th mode of Double Harmonic Major" },
    [ 871] = { "Locrian bb3 bb7", "7th mode of Double Harmonic Major" },
};




/* ==== Print ==== */

void print_set_description(Set set, Description* table, u64 table_count) {

    u64 index = set.data;
    assert(index < table_count);

    Description d = table[index];
    if (d.name)         printf("%s",  d.name);
    if (d.description)  printf(": %s", d.description);
}

void print_set_12tet(Set set) {
    assert(set.size == 12);
    print_set(set);
    printf(" ");
    print_set_description(set, description_table_12tet, SET_12TET_COUNT);
    printf("\n");
}

// todo: make this work better
void print_set_traditional(Set s) {

    assert(s.size == 12);
    print_set_12tet(s);

    u8  set[64] = {0};
    u64 count   = set_to_array(s, set);

    if (set[0] != 0) goto print_naive;
    if (count  != 7) goto print_naive;

    // now it's a 7 note normal set

    static char* note_names_major    = "CDEFGAB";
    static u8    note_values_major[] = { 0, 2, 4, 5, 7, 9, 11 };

    /* ---- find max diff ---- */
    {
        s32 max_diff = 0;

        for (u64 i = 0; i < count; i++) {

            s32 diff = (s32) set[i] - (s32) note_values_major[i];

            diff     = diff < 0 ? -diff : diff;
            max_diff = diff > max_diff ? diff : max_diff;
        }

        if (max_diff > 2) goto print_naive; // too much sharp/flats
    }

    /* ---- print 7 note scale ---- */
    {
        printf("Absolute: ");
        for (u64 i = 0; i < count; i++) {

            u8 it   = set[i];
            u8 note = note_values_major[i];

            putchar(note_names_major[i]);
            for (u64 j = it; j < note; j++) putchar('b');
            for (u64 j = note; j < it; j++) putchar('#');
            putchar(' ');
        }
        printf("\n");

        printf("Relative: ");
        for (u64 i = 0; i < count; i++) {

            u8 it   = set[i];
            u8 note = note_values_major[i];

            for (u64 j = it; j < note; j++) putchar('b');
            for (u64 j = note; j < it; j++) putchar('#');
            printf("%llu ", i + 1);
        }

        printf("\n\n");

        return;
    }

    print_naive:
    {
        static char* note_names1[] = { "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B" };
        static char* note_names2[] = { "1", "b2", "2", "b3", "3", "4", "b5", "5", "b6", "6", "b7", "7" };

        printf("Absolute: ");
        for (u64 i = 0; i < count; i++) {
            printf("%s ", note_names1[set[i]]);
        }
        printf("\n");

        printf("Relative: ");
        for (u64 i = 0; i < count; i++) {
            printf("%s ", note_names2[set[i]]);
        }

        printf("\n\n");
    }
}




/* ==== Stats ==== */

u64 set_to_array_and_check_is_12tet_normal(Set s, u8 out[64]) {
    assert(s.size == 12);
    assert(s.data & 1);
    return set_to_array(s, out);
}

//  1 2 3 4  5  6  7 8  9 10 11
//  to
//  7 2 9 4 11  6  5 10 3 8  1
void set_oiv_index_order_to_ic7_order(u64 oiv[11]) {

    u64 copy[11];
    for (u64 i = 0; i < 11; i++) copy[i] = oiv[i];

    oiv[0]  = copy[6];
    oiv[1]  = copy[1];
    oiv[2]  = copy[8];
    oiv[3]  = copy[3];
    oiv[4]  = copy[10];
    oiv[5]  = copy[5];
    oiv[6]  = copy[4];
    oiv[7]  = copy[9];
    oiv[8]  = copy[2];
    oiv[9]  = copy[7];
    oiv[10] = copy[0];
}

void set_get_oiv(Set s, u64 out[11]) {

    u8  set[64] = {0};
    u64 count   = set_to_array_and_check_is_12tet_normal(s, set);

    for (u64 i = 0; i < 11; i++) {
        out[i] = 0;
    }

    if (count < 2) return;

    for (u64 i = 0; i < count; i++) {
        for (u64 j = i + 1; j < count; j++) {
            u64 index = set[j] - set[i];
            assert(index > 0 && index <= 11);
            out[index - 1]++;
        }
    }
}

void set_get_uiv(Set s, u64 out[6]) {

    u8  set[64] = {0};
    u64 count   = set_to_array_and_check_is_12tet_normal(s, set);

    for (u64 i = 0; i < 6; i++) {
        out[i] = 0;
    }

    if (count < 2) return;

    for (u64 i = 0; i < count; i++) {
        for (u64 j = i + 1; j < count; j++) {

            u64 index = set[j] - set[i];
            assert(index <= 11);

            if (index > 6) index = 12 - index;

            assert(index > 0 && index <= 6);
            out[index - 1]++;
        }
    }
}

//  0 6   7 2 9 4 11   5 10 3 8  1
//  to
//  0 1   2 3 4 5 6    7 8  9 10 11
void set_pv_weighting_ic7_order_to_index_order(s64 weighting[12]) {

    s64 copy[12];
    for (u64 i = 0; i < 12; i++) copy[i] = weighting[i];

    weighting[0 ]  = copy[0];
    weighting[6 ]  = copy[1];
    weighting[7 ]  = copy[2];
    weighting[2 ]  = copy[3];
    weighting[9 ]  = copy[4];
    weighting[4 ]  = copy[5];
    weighting[11]  = copy[6];
    weighting[5 ]  = copy[7];
    weighting[10]  = copy[8];
    weighting[3 ]  = copy[9];
    weighting[8 ]  = copy[10];
    weighting[1 ]  = copy[11];
}

s64 set_get_pv_from_weighting(Set s, s64 weighting[12]) {

    u8  set[64] = {0};
    u64 count   = set_to_array_and_check_is_12tet_normal(s, set);

    s64 out = 0;
    for (u64 i = 0; i < count; i++) {
        u64 index = set[i];
        assert(index < 12);
        out += weighting[index];
    }

    return out;
}

// todo: cleanup
u8 set_is_sparse(Set s) {

    u8 set[64] = {0};
    u64 count = set_to_array_and_check_is_12tet_normal(s, set);

    if (count < 3) return 1;

    for (u64 i = 0; i < count - 2; i++) {

        u8 a = set[i];
        u8 b = set[i + 1];
        u8 c = set[i + 2];

        if (b - a < 2 && c - b < 2) return 0;
    }

    {
        u8 a = set[count - 2];
        u8 b = set[count - 1];
        u8 c = set[0] + 12;
        if (b - a < 2 && c - b < 2) return 0;
    }

    {
        u8 a = set[count - 1];
        u8 b = set[0] + 12;
        u8 c = set[1] + 12;
        if (b - a < 2 && c - b < 2) return 0;
    }

    return 1;
}

// todo: cleanup
u8 set_is_pure_tertian(Set s) {

    u8  set[64] = {0};
    u64 count   = set_to_array_and_check_is_12tet_normal(s, set);

    if (count < 3) return 0;

    for (u64 i = 0; i < count - 2; i++) {

        u8 a = set[i];
        u8 b = set[i + 2];

        u8 d = b - a;
        if (!(d == 3 || d == 4)) return 0;
    }

    for (u64 i = 0; i < 2; i++) {

        u8 a = set[count - 2 + i];
        u8 b = set[i] + 12;

        u8 d = b - a;
        if (!(d == 3 || d == 4)) return 0;
    }

    return 1;
}

