/* ==== Macros ==== */

#define string(s)            (String) {(u8*) s, sizeof(s) - 1}
#define array_string(s)      (String) {(u8*) s, sizeof(s)}
#define data_string(s)       (String) {(u8*) &s, sizeof(s)}
#define count_of(...)        (sizeof(__VA_ARGS__) / sizeof(__VA_ARGS__[0]))



/* ==== Types ==== */

typedef unsigned char           u8;
typedef unsigned short int      u16;
typedef unsigned int            u32;
typedef unsigned long long int  u64;
typedef signed char             s8;
typedef signed short            s16;
typedef signed int              s32;
typedef signed long long int    s64;
typedef float                   f32;
typedef double                  f64;

typedef struct {
    u8* data;
    u64 count;
} String;




/* ==== File ==== */

String load_file(char* path) {

    FILE* f = fopen(path, "rb");
    if (!f) return (String) {0};

    fseek(f, 0, SEEK_END);
    u64 count = (u64) ftell(f);
    fseek(f, 0, SEEK_SET);

    u8* data = malloc(count);
    if (!data) {
        fclose(f);
        return (String) {0};
    }

    fread(data, 1, count, f);
    fclose(f);

    return (String) {data, count};
}

u8 save_file(String in, char* path) {

    FILE* f = fopen(path, "wb");
    if (!f) return 0;

    fwrite(in.data, sizeof(u8), in.count, f);
    fflush(f);
    fclose(f);

    return 1;
}

