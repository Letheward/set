/* ==== Misc Utils ==== */

#define ONE_64 ((u64) 1)  // sigh...

typedef struct {
    char* name;
    char* description;
} Description;

typedef struct {
    s64 start;
    s64 end;
} Range;




/* ==== Set ==== */

// todo: there are probably faster ways to do these binary stuff in Hackers' Delight

typedef struct {
    u64 data; // binary form of set
    u64 size; // the order of the field (n in 2^n) the set belongs to, e.g. 12 for pitches
} Set;



/* ---- Utils ---- */

// note: use it like this: set_make(12, {0, 7, 2, 9, 4})
#define set_make(size, ...) set_make_helper(size, (u64 []) __VA_ARGS__, count_of((u64 []) __VA_ARGS__))
Set set_make_helper(u64 size, u64* data, u64 count) {

    assert(size <= 64);

    u64 out = 0;

    for (u64 i = 0; i < count; i++) {
        u64 value = data[i];
        assert(value < size);
        out |= ONE_64 << value;
    }

    return (Set) { out, size };
}

u64 set_to_array(Set set, u8 out[64]) {

    assert(set.size <= 64);

    u64 count = 0;

    for (u64 i = 0; i < set.size; i++) {
        if (set.data & (ONE_64 << i)) {
            out[count] = (u8) i;
            count++;
        }
    }

    return count;
}

u64 set_get_mask(Set s) {
    assert(s.size > 0 && s.size <= 64);
    return (ONE_64 << s.size) - 1;
}

// note: this is the cardinal of the set itself, not the order of the field
u64 set_get_size(Set s) {

    u64 size = 0;

    for (u64 x = s.data; x; x >>= 1) {
        size += x & 1;
    }

    assert(size <= s.size);

    return size;
}


void print_set(Set set) {

    u64 index = set.data;

    // binary
    printf("%llu [", set.size);
    for (u64 i = set.size; i > 0;) {
        i--;
        putchar(index & (ONE_64 << i) ? '1' : '0');
    }

    // decimal and set
    char* format = " | %llu] ";
    if (set.size >  0  && set.size <= 4)  format = " | %.2llu] ";
    if (set.size >= 5  && set.size <= 9)  format = " | %.3llu] ";
    if (set.size >= 10 && set.size <= 13) format = " | %.4llu] ";
    printf(format, set.data);

    printf("[%llu] { ", set_get_size(set));
    for (u64 i = 0; i < set.size; i++) {
        if (index & (ONE_64 << i)) printf("%llu ", i);
    }
    printf("}");
}

void print_set_line(Set set) {
    print_set(set);
    printf("\n");
}




/* ---- Core ---- */

u8 set_equal(Set a, Set b) {
    return a.data == b.data && a.size == b.size;
}

Set set_add(Set a, Set b) {
    assert(a.size == b.size);
    return (Set) { (a.data + b.data) & set_get_mask(a), a.size };
}

// todo: validate that underflow did the correct thing
Set set_sub(Set a, Set b) {
    assert(a.size == b.size);
    return (Set) { (a.data - b.data) & set_get_mask(a), a.size };
}

Set set_mul(Set a, Set b) {
    assert(a.size == b.size);
    return (Set) { (a.data * b.data) & set_get_mask(a), a.size };
}

Set set_div(Set a, Set b) {
    assert(a.size == b.size);
    assert(b.data != 0);
    return (Set) { (a.data / b.data) & set_get_mask(a), a.size };
}

Set set_mod(Set a, Set b) {
    assert(a.size == b.size);
    assert(b.data != 0);
    return (Set) { (a.data % b.data) & set_get_mask(a), a.size };
}

Set set_and(Set a, Set b) {
    assert(a.size == b.size);
    return (Set) { (a.data & b.data) & set_get_mask(a), a.size };
}

Set set_or(Set a, Set b) {
    assert(a.size == b.size);
    return (Set) { (a.data | b.data) & set_get_mask(a), a.size };
}

Set set_xor(Set a, Set b) {
    assert(a.size == b.size);
    return (Set) { (a.data ^ b.data) & set_get_mask(a), a.size };
}

Set set_not(Set a) {
    return (Set) { ~a.data & set_get_mask(a), a.size };
}



/* ---- Transforms and Stats ---- */

// this is just "bit rotate", but in the range of s.size
Set set_rotate(Set s, s64 amount) {

    u64 x = s.data;

    if (amount < 0) {
        u64 a = ((u64) -amount) % s.size;
        x = (x >> a) | (x << (s.size - a));
    }

    if (amount > 0) {
        u64 a = ((u64) amount) % s.size;
        x = (x << a) | (x >> (s.size - a));
    }

    return (Set) { x & set_get_mask(s), s.size };
}

// mirror around the 0 axis
Set set_mirror(Set s) {

    u64 out = s.data & 1;

    for (u64 i = 1; i < s.size; i++) {
        if (s.data & (ONE_64 << i)) {
            out |= (ONE_64 << (s.size - i));
        }
    }

    return (Set) { out, s.size };
}

Set set_normalize(Set s) {
    u64 out = s.data;
    while (out && !(out & 1)) out >>= 1;
    return (Set) { out, s.size };
}

u8 set_is_normalized(Set s) {
    return s.data & 1;
}

u8 set_is_neutral(Set s) {
    return set_equal(s, set_mirror(s));
}

u64 set_get_diff_count(Set a, Set b) {

    u64 diff = set_xor(a, b).data;

    u64 out = 0;
    for (u64 i = 0; i < 64; i++) {
        if (diff & (ONE_64 << i)) out++;
    }

    return out;
}

u8 set_is_close_to(Set a, Set b, Range* ranges, u64 range_count) {

    assert(a.size == b.size);

    u8  va[64]  = {0};
    u8  vb[64]  = {0};
    u64 a_count = set_to_array(a, va);
    u64 b_count = set_to_array(b, vb);

    if (a_count != b_count)     return 0;
    if (a_count != range_count) return 0;

    for (u64 i = 0; i < a_count; i++) {

        Range range = ranges[i];
        s64 diff  = (s64) va[i] - (s64) vb[i];

        if (diff < range.start || diff > range.end) return 0;
    }

    return 1;
}




/* ---- Prime Forms and Modes ---- */

// todo: validate
// note:
//     this is different from Forte and Rahn, since it doesn't consider inversions of sets same sets,
//     for example, {0, 4, 7} and {0, 3, 7} are both prime forms,
//     but only {0, 3, 7} is prime form in Forte and Rahn's conventions
Set set_to_prime_form(Set s) {

    u64 x = set_normalize(s).data;

    for (u64 i = 0; i < s.size; i++) {
        u64 k = set_rotate(s, (s64) i).data;
        if (k & 1 && k < x) x = k;
    }

    return (Set) { x, s.size };
}

// todo: validate
// this is prime form in Rahn's convention?
Set set_to_prime_form_consider_inversion(Set s) {

    u64 a = set_to_prime_form(s).data;
    u64 b = set_to_prime_form(set_mirror(s)).data;
    if (b < a) a = b;

    return (Set) { a, s.size };
}

// todo: validate
u64 set_get_all_modes(Set s, u64 modes[64]) {

    s = set_to_prime_form(s); // todo: maybe slow?

    u64 count = 0;
    for (u64 i = 0; i < 64; i++) {

        u64 mode = set_rotate(s, -(s64) i).data;
        if (!(mode & 1)) continue;

        // linear search dedup
        u8 found = 0;
        for (u64 j = 0; j < count; j++) {
            if (mode == modes[j]) {
                found = 1;
                break;
            }
        }

        if (!found) {
            modes[count] = mode;
            count++;
        }
    }

    return count;
}

