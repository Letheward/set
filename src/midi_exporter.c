/* ==== MIDI Exporter ==== */

// todo: figure out how to append rest notes

/*

A simple .mid file

4d 54 68 64      MThd
00 00 00 06      chunk count
00 01
00 01
01 e0

4d 54 72 6b      MTrk
00 00 00 2c      chunk count
00 ff 03 00      name

    note on            note off
00  90 48 66   83 60   80 48 66
00  90 48 66   83 60   80 48 66
00  90 48 66   83 60   80 48 66
00  90 48 66   83 60   80 48 66

00  ff 2f 00     end

*/

typedef struct {

    u8* data;
    u64 count;
    u64 allocated;

    u64 whole_note_time;
    u64 chunk_count;
    u64 chunk_count_position;

} MIDI_Exporter;

u16 swap_endian_u16(u16 in) {
    return (
        (in & 0x00ff) << 8 |
        (in & 0xff00) >> 8
    );
}

u32 swap_endian_u32(u32 in) {
    return (
        (in & 0x000000ff) << 24 |
        (in & 0x0000ff00) << 8  |
        (in & 0x00ff0000) >> 8  |
        (in & 0xff000000) >> 24
    );
}

// todo: validate and cleanup
u64 u32_to_variable_length(u32 in, u8 out[4]) {

    u8  temp[4] = {0};
    u64 count   = 0;

    temp[0] = (u8) (in & 0x7f);
    if (in < 0x80) {

        count = 1;

    } else {

        for (u64 i = 1; i < 4; i++) {
            in >>= 7;
            temp[i] = (u8) (in & 0x7f) | 0x80;
            if (in < 0x80) {
                count = i + 1;
                break;
            }
        }
    }

    for (u64 i = 0; i < count; i++) {
        out[count - i - 1] = temp[i];
    }

    return count;
}

String midi_exporter_string(MIDI_Exporter* e) {
    return (String) { e->data, e->count };
}

u64 midi_exporter_append(MIDI_Exporter* e, String s) {

    u64 wanted = e->count + s.count;
    while (wanted > e->allocated) {
        e->data = realloc(e->data, e->allocated * 2);
        assert(e->data);
        e->allocated *= 2;
    }

    memcpy(&e->data[e->count], s.data, s.count);
    e->count = wanted;

    return s.count;
}

void midi_exporter_start(MIDI_Exporter* e, u64 count, u64 quarter_note_time) {

    *e = (MIDI_Exporter) {
        .data            = calloc(count, sizeof(u8)),
        .allocated       = count,
        .whole_note_time = quarter_note_time * 4,
    };

    assert(e->data != NULL);

    {
        u16 time = swap_endian_u16((u16) quarter_note_time);

        u8 info[] = {
            0x00, 0x00, 0x00, 0x06,
            0x00, 0x01,
            0x00, 0x01,
        };

        e->chunk_count_position += midi_exporter_append(e, string("MThd"));
        e->chunk_count_position += midi_exporter_append(e, array_string(info));
        e->chunk_count_position += midi_exporter_append(e, data_string(time));
    }

    {
        u32 length = 0; // placeholder
        u8  info[] = { 0x00, 0xff, 0x03, 0x00 };

        e->chunk_count_position += midi_exporter_append(e, string("MTrk"));

        midi_exporter_append(e, data_string(length));
        e->chunk_count += midi_exporter_append(e, array_string(info));
    }
}

void midi_exporter_end(MIDI_Exporter* e) {

    u8 end[] = { 0x00, 0xff, 0x2f, 0x00 };
    e->chunk_count += midi_exporter_append(e, array_string(end));

    u32* p = (u32*) (e->data + e->chunk_count_position); // for MTrk chunk's length
    *p = swap_endian_u32((u32) e->chunk_count);
}

void midi_exporter_append_note(MIDI_Exporter* e, u8 note, u32 division) {

    u8  time[4]    = {0};
    u64 time_count = u32_to_variable_length((u32) e->whole_note_time / division, time);

    u8 on[]  = { 0x00, 0x90, note, 0x66 };
    u8 off[] = {       0x80, note, 0x66 };

    e->chunk_count += midi_exporter_append(e, array_string(on));
    e->chunk_count += midi_exporter_append(e, (String) { time, time_count });
    e->chunk_count += midi_exporter_append(e, array_string(off));
}

void midi_exporter_append_set_arp(MIDI_Exporter* e, Set s, u32 division) {

    assert(s.size == 12);

    u8  time[4]    = {0};
    u64 time_count = u32_to_variable_length((u32) e->whole_note_time / division, time);

    u64 index = s.data;

    for (u64 i = 0; i < s.size; i++) {

        if (!(index & (ONE_64 << i))) continue;

        u8 on[]  = { 0x00, 0x90, 60 + (u8) i, 0x66 };
        u8 off[] = {       0x80, 60 + (u8) i, 0x66 };

        e->chunk_count += midi_exporter_append(e, array_string(on));
        e->chunk_count += midi_exporter_append(e, (String) { time, time_count });
        e->chunk_count += midi_exporter_append(e, array_string(off));
    }
}

// todo: cleanup
void midi_exporter_append_set_chord(MIDI_Exporter* e, Set s, u32 division) {

    assert(s.size == 12);

    u8  time[4]    = {0};
    u64 time_count = u32_to_variable_length((u32) e->whole_note_time / division, time);

    u64 index = s.data;

    for (u64 i = 0; i < s.size; i++) {
        if (!(index & (ONE_64 << i))) continue;
        u8 on[] = { 0x00, 0x90, 60 + (u8) i, 0x66 };
        e->chunk_count += midi_exporter_append(e, array_string(on));
    }

    u64 i;
    for (i = 0; i < s.size; i++) {
        if (index & (ONE_64 << i)) {
            u8 dt[] = { 0x80, 60 + (u8) i, 0x66 };
            e->chunk_count += midi_exporter_append(e, (String) { time, time_count });
            e->chunk_count += midi_exporter_append(e, array_string(dt));
            break;
        }
    }

    for (; i < s.size; i++) {
        if (!(index & (ONE_64 << i))) continue;
        u8 off[] = { 0x00, 0x80, 60 + (u8) i, 0x66 };
        e->chunk_count += midi_exporter_append(e, array_string(off));
    }
}

